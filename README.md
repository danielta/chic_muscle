## Example how to couple C++ models using Muscle2/Mapper framework
This example creates a sender/receiver coupled models. They exchange an array of raw data and an integer.
Sender sends a block of data and an integer number, Receiver receives them, increments the integer and sends back the updated data.
The output on the console shows the updating of this integer (called receiver_timestep).

To setup the communication topology Muscle needs a topology file: 
```
#!bash

src/chic_sender_receiver.cxa.rb
```
It is used by Muscle to couple the different instances (model executables) that communicate each other through ports.
If you need more ports just add them in the configuration file and in the source code as well.
In the configuration file you can set global variables (i.e. environment variables) that can be retrieved from code using muscle functions.

Please see in code documentation, in particular the communications are handled in the `do_communications()` function.


### Install and run the example ###

#### 1 - Install MUSCLE2 following instructions 
  [MUSCLE2](http://apps.man.poznan.pl/trac/muscle/wiki/Installation)

  once installation is terminated source the muscle.profile from your ~/.bashrc or ~/.profile depending on your platform.
  This configuration will create the environment variable MUSCLE_HOME pointing to the MUSCLE installation

#### 2 - Clone this repository
```  
 git clone https://danielta@bitbucket.org/danielta/chic_muscle.git

    cd chic_muscle
    
    cmake .
    
    make install
```


#### 3 - Run the example, as sender/receiver

```    muscle2 -mac src/chic_sender_receiver.cxa.rb ```

note that sender and receiver are running in parallel so the output on the console will not appear sorted by time. In square brackets you can see the time since Epoch to check the order of execution.

#### 4 - If you want to improve the code fork it and create a pull request when you are ready.
Keep updated your copy with the frequent changes.

The following graduation workflow is adopted in this git repository:

    <topic_branch>-->dev-->master

### Muscle reference API and documentation ###
[MUSCLE2 Documentation and API](http://apps.man.poznan.pl/trac/muscle/wiki/C%2B%2B%20API)


----------

#####  Daniele Tartarini