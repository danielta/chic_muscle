cmake_minimum_required(VERSION 2.8)

project(template_chic CXX)


# Enable C++11 compilation
list( APPEND CMAKE_CXX_FLAGS "-std=c++0x ${CMAKE_CXX_FLAGS}" )

# Add binary instances to be executed
add_executable(model_sender model_sender.cpp)
add_executable(model_receiver model_receiver.cpp)

# Find MUSCLE2 TODO
# find_package(MUSCLE2 REQUIRED)


# Get directory where muscle is installed. defined in muscle.profile
set (MUSCLE_dir $ENV{MUSCLE_HOME})

message ("Using Muscle in folder= ${MUSCLE_dir}")

set (DESTINATION_dir "${PROJECT_SOURCE_DIR}/bin")

# Set include directories
include_directories("${MUSCLE_dir}/include/")

# Bind to muscle library
set(MUSCLE2_LIB "${MUSCLE_dir}/lib/libmuscle2.so")

# Link executables with libraries
target_link_libraries (model_sender  ${MUSCLE2_LIB})
target_link_libraries (model_receiver  ${MUSCLE2_LIB})

# Install targets in destination directory
install(TARGETS model_sender model_receiver DESTINATION ${DESTINATION_dir}/${PROJECT_NAME})

