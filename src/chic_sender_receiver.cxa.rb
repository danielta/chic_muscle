# Parameters declared here as $env can be queried using Muscle library in from instances

$env['steps'] = 5
$env['preparation_steps'] = 10
$env['sender_input_file'] = 'sender_input_file.txt'
$env['sender_output_file'] = 'sender_output_file.txt'

$env['receiver_input_file'] = 'receiver_input_file.txt'
$env['receiver_output_file'] = 'receiver_output_file.txt'


$env['GC_cell_x'] = 5
$env['GC_cell_y'] = 5
$env['GC_cell_z'] = 5

# configure cxa properties
$env['max_timesteps'] = 20;
$env['default_dt'] = 1

# Check Muscle environment is loaded
abort "Run 'source [MUSCLE_HOME]/etc/muscle.profile' before this script" if not ENV.has_key?('MUSCLE_HOME')

#dir = ENV['MUSCLE_HOME'] + ''path/to/binary

#dir = '/home/tartarini/DEVELOPMENT/CHIC/CHIC_MUSCLE/src/local'

# Path to the binaries of the models to be coupled
dir ='./src/bin/template_chic'

# Declare kernels (model binaries) that will be called from Muscle
sender = NativeInstance.new('sender', "#{dir}/model_sender" )
receiver = NativeInstance.new('receiver', "#{dir}/model_receiver" )

# configure connection scheme
sender.couple(   receiver, 'out' => 'in')
receiver.couple( sender,   'out' => 'in')
sender.couple(   receiver, 'out_timestep' => 'in_timestep')
receiver.couple(  sender,  'out_timestep' => 'in_timestep')

# Value assigned to a single kernel
#sender['property'] = 'property_value'
# $env['sender:property_name'= property_value

# example of call to instances
# w = NativeInstance.new('w', '/path/to/w', args: 'param1 param2')
# w = PythonInstance.new('w', 'myscript.py')

