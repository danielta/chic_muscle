#ifndef CHIC_UTILS
#define CHIC_UTILS

#include <iostream>
#include <climits>
#include <cstring>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include <muscle2/cppmuscle.hpp>
#include <muscle2/util/exception.hpp>
#include <exception>

using namespace muscle;
using namespace std;

/* \function initialise_JVM
*  \brief set of commands used by Muscle to initialise connections in the Java Virtual
*         machine
*/
int initialise_JVM(int prepare_steps, int runs, size_t preparation_size = 1024)
{
    // Print instance name
    cout << "Kernel Name: " << muscle::cxa::kernel_name() << endl;

    long *totalTimes = new long[runs];
    void *data = malloc(preparation_size);

    if (!data)
    {	  throw muscle_exception("Can not allocate data to warm up environment.");
    }

    // MUSCLE code to initialise JVM
	for (int i = 0; i < prepare_steps; i++)
	{
	  env::send("out", data, preparation_size, MUSCLE_RAW);
	  env::receive("in", data, preparation_size, MUSCLE_RAW);
	  if (i % 5 == 0)
        cout << "S.";
    }
    free(data);
}

#endif
