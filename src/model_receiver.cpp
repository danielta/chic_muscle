/*
* Copyright 2010-2013 Multiscale Applications on European e-Infrastructures (MAPPER) project
*
* GNU Lesser General Public License
*
* This file is part of MUSCLE (Multiscale Coupling Library and Environment).
*
* MUSCLE is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* MUSCLE is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with MUSCLE.  If not, see <http://www.gnu.org/licenses/>.
* ---------------------------------------------------------------------------------------
* The original version of this file been modified from the following contributors
* and is not part of Muscle:
*   2015: Daniele Tartarini
*/

#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <muscle2/cppmuscle.hpp>
#include <exception>
#include <chrono>
#include "chic_utils.h"


using namespace muscle;
using namespace std;
using namespace std::chrono;

/// Do computation and communications
void do_communications(void)
{
    // Declare pointer to buffer hosting data to be received
    void *data = (void *)0;

    size_t count;
    // Define timestep and its buffer size
    long int time_step = 0;
    size_t timestep_size =1;

    // Receive raw data in buffer
    data = env::receive("in", data, count, MUSCLE_RAW);

    // Receive integer number from sender
    time_step = *(long*)env::receive("in_timestep", &time_step, timestep_size, MUSCLE_INT64);

    // Taking time since epoch
    auto time_now = high_resolution_clock::now();
    long int clock_time = time_now.time_since_epoch().count();
    std::cout << "\n[" << clock_time << "] Receiver: received time_step=" << time_step;

    // Do some computation incrementing the received integer
    time_step++;

    // Send back the incremented number
    env::send("out_timestep", &time_step, 1, MUSCLE_INT64);

    // Send data back to Sender
    env::send("out", data, count, MUSCLE_RAW);

    // Free MUSCLE buffer
    env::free_data(data, MUSCLE_RAW);
}

int main(int argc, char **argv)
{
	try
	{

    // Initialise MUSCLE2 environment
    env::init(&argc, &argv);

    // Print identity of running instance
    cout << "Kernel Name: " << muscle::cxa::kernel_name() << endl;

    // How many time steps for the whole simulation
    const int max_timesteps = atoi(cxa::get_property("max_timesteps").c_str());

    // Number of steps to warm up the system
    const int prepare_steps = atoi(cxa::get_property("preparation_steps").c_str());

    // Get input/output filenames
    std::string input_filename = cxa::get_property("receiver_input_file");
    std::cout<< "\nReceiver Input filename = " << input_filename << std::endl;
    std::string output_filename = cxa::get_property("receiver_output_file");
    std::cout<< "\nReceiver Output filename = " << output_filename << std::endl;

    // Helper function to initialise JVM communications
    std::cout << "Initialisation Receiver";
    size_t preparation_size = 1024;
    initialise_JVM(prepare_steps, prepare_steps, preparation_size);

    // Local time loop
    for (int i = 0; i < max_timesteps; i++)
		{
        do_communications();
		}

		// Finalise Muscle environment and cleanup
		env::finalize();
	}
	catch(exception& ex)
	{
		logger::severe("Error occurred in Sender: %s", ex.what());
	}
	catch(...)
	{
		cerr << "Error occured in Receiver" << endl;
	}

	return 0;
}
