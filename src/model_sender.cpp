/*
* Copyright 2010-2013 Multiscale Applications on European e-Infrastructures (MAPPER) project
*
* GNU Lesser General Public License
*
* This file is part of MUSCLE (Multiscale Coupling Library and Environment).
*
* MUSCLE is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* MUSCLE is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with MUSCLE.  If not, see <http://www.gnu.org/licenses/>.
* ---------------------------------------------------------------------------------------
* The original version of this file been modified from the following contributors
* and is not part of Muscle:
*   2015: Daniele Tartarini
*/


#include <iostream>
#include <climits>
#include <cstring>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include <muscle2/cppmuscle.hpp>
#include <muscle2/util/exception.hpp>
#include <exception>
#include <chrono>
#include "chic_utils.h"

using namespace std::chrono;
using namespace muscle;
using namespace std;

/*
* \brief send an integer to receiver and receives it incremented. Send a buffer of raw data
*        that can be used to send a multidimensional array
* \param buffer_size is the size of buffer for matrix data communication
*
*/
long do_communications(const size_t size, const int max_timesteps, long int& receiver_timestep)
{

    size_t buffer_size = size;

    // Allocate a buffer to receive raw data
	  void *buffer = malloc(buffer_size);
	  if (!buffer)
	  {
		    throw muscle_exception("Could not allocate buffer to send message.");
	  }

    // Get number of elements in the matrix
    size_t matrix_num_elements = buffer_size/sizeof(double);

/*    double *matrix = (double*)buffer;

	  // Initialise a fake matrix to be transferred
	  for (int i=0; i< buffer_size; i++)
	  {
	      matrix[i] = i;
	  }
*/

    size_t timestep_size = 1;

    // Send data to output ports
    env::send("out", buffer, buffer_size, MUSCLE_RAW);

    // Send the counter to receiver to be incremented
    env::send("out_timestep", &receiver_timestep, timestep_size, MUSCLE_INT64);

    // Taking time since epoch
    auto time_now = high_resolution_clock::now();
    long int clock_time = time_now.time_since_epoch().count();

    std::cout << "\n[" << clock_time << "] Sender sends timestep=" << receiver_timestep;

    // Receive data from input ports
    // Receive raw data in the buffer
    env::receive("in", buffer, buffer_size, MUSCLE_RAW);

    // Receive the counter increased by a unit
    env::receive("in_timestep", &receiver_timestep, timestep_size, MUSCLE_INT64);
    std::cout <<"  >> receives timestep=" << receiver_timestep;


    // Add linefeed for visual convenience
    std::cout << std::endl;

    // free buffer
    free(buffer);
    return receiver_timestep;
}




int main(int argc, char **argv)
{
    try
	  {

        // Get reference to command line parameters
		    env::init(&argc, &argv);

        // Print instance name
        cout << "Kernel Name: " << muscle::cxa::kernel_name() << endl;

        // Number of steps to warm up the system (MUSCLE requirement)
        const int prepare_steps = atoi(cxa::get_property("preparation_steps").c_str());

        // Get input/output filenames
        std::string input_filename = cxa::get_property("sender_input_file");
        std::cout<< "\nInput filename = " << input_filename << std::endl;
        std::string output_filename = cxa::get_property("sender_output_file");
        std::cout<< "\nOutput filename = " << output_filename << std::endl;

        // How many steps total will be done
        const int max_timesteps = atoi(cxa::get_property("max_timesteps").c_str());

        // Helper function to initialise JVM communications
        initialise_JVM(prepare_steps, prepare_steps, 1024);

        const size_t message_buffer_size = 1024;

        // A counter to be incremented by model_receiver
        long int receiver_timestep=0;

        // main time loop
	      for(int i = 0; i<max_timesteps; i++)
        {

            long num_iterations = do_communications( message_buffer_size, max_timesteps,
                                    receiver_timestep);

        }
        // Finalise
        env::finalize();


    }
	  catch(exception& ex)
	  {
		    logger::severe("Error occurred in sending: %s", ex.what());
	  }
    catch(...)
	  {
		    cerr << "Error occured in Sending" << endl;
	  }

	return 0;
}
